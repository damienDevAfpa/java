public class Kiwi extends Fruit implements PeelFruit {

    public Kiwi(String name) {
        super("kiwi");
    }

    @Override
    public void taste() {
        System.out.println("le gout est plutôt sucré");
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public boolean hasSeed() {
        return false;
    }

    @Override
    public void miam() {
        super.miam();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    // PeelFruit
    @Override
    public boolean isPeeled() {
        return true;
    }

    @Override
    public String getSkinType() {
        return "doux";
    }
}
