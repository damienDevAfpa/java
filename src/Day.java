public enum Day {

    MATIN(8, "il l'heure de se lever"),
    MIDI(12, "A table"),
    APRESMIDI(15, "bon jeu"),
    SOIR(22, "bonne nuit"),
    NUIT(2, "fais de beau reve");


    private int hour;
    private String message;

    Day(int hour, String message){
        this.hour = hour;
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public int getHour(){
        return hour;
    }

}
