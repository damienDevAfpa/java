// abstract = on doit redéfinir certaines propriétées / méthode
public abstract class Fruit  {
    private String name;

    public Fruit(String name){
        this.name = name;
    }

    // taste() sera redéfinie dans une classe fille
    public abstract void taste();

    public abstract int getSize();

    public abstract boolean hasSeed();

    public void miam(){
        System.out.println("Fruit :" + getName());
    }

    public String getName(){
        return name;
    }

}
