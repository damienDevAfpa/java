public class Apple extends Fruit   {

   public Apple(){
       super("pomme");
   }

  /*  @Override
    public boolean isPeeled() {
        return false;
    }

    @Override
    public String getSkinType() {
        return null;
    }*/

    @Override
    public void taste() {
        System.out.println("La pomme à un gout plutôt sucré");
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public boolean hasSeed() {
        return true;
    }
}
