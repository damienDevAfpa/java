public class Main {

    public static void main(String[] args) {

        int[] numbers = {4, 8 ,12 ,18};
        

        // boucle sur chacun des éléments
       for (Day moment : Day.values()) {
           if (!moment.equals(Day.MIDI)){
               System.out.println( "Nous somme le " + moment.name() +
                       " il est actuellement :" + moment.getHour() + " heure, "+ moment.getMessage());
           }

       }

        Apple a = new Apple();
        a.taste();
        a.miam();

        Pineapple pen = new Pineapple();
        pen.taste();
        pen.miam();

        if (!(a instanceof PeelFruit)) {
            System.out.println("ce fruit n'implemente pas l'interface PeelFruit");
        }
        else {
            System.out.println("Ce fruit implémente l'interface PeelFruit");
        }


    }

    //  variable globale =
    // static int number = 5;
    // ignorer une iteration = continue;
    // interrompre une sequence d'action = break;
}

