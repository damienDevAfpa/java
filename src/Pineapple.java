public class Pineapple extends Fruit implements PeelFruit {

    private boolean peeled = false;

    // Super class Fruit
    public Pineapple( ){
        super("ananas");
    }

    @Override
    public void taste() {
        System.out.println("L'annanas à un gout plutôt acide");
    }

    @Override
    public int getSize() {
        return 2;
    }

    @Override
    public boolean hasSeed() {
        return false;
    }

    // PeelFruit
    @Override
    public boolean isPeeled() {
        return peeled;
    }

    @Override
    public String getSkinType() {
        return "piquante";
    }

}
