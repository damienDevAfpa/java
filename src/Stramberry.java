
public  class Stramberry extends Fruit implements PeelFruit {

    public Stramberry(String name) {
        super("fraise");
    }

    @Override
    public boolean isPeeled() {
        return false;
    }

    @Override
    public String getSkinType() {
        return null;
    }

    @Override
    public void taste() {
    System.out.println("Le gout est plutôt sucré");
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public boolean hasSeed() {
        return false;
    }
}
